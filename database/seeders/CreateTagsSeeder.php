<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class CreateTagsSeeder extends Seeder
{
    const TAGS_COUNT_NEED_TO_CREATE = 10;

    public function run()
    {
        Tag::factory(self::TAGS_COUNT_NEED_TO_CREATE)->create();
    }
}
