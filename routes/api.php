<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('posts')
    ->namespace('Post')
    ->as('posts.')
    ->group(function () {

        Route::get('', 'PostController@list')
            ->name('list');
        Route::post('', 'PostController@create')
            ->name('create');
        Route::put('/{id}', 'PostController@update')
            ->name('update');
        Route::delete('/{id}', 'PostController@delete')
            ->name('delete');
    });
