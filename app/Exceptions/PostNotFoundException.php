<?php

namespace App\Exceptions;

class PostNotFoundException extends BusinessLogicException
{
    public function getStatus(): int
    {
        return BusinessLogicException::POST_NOT_FOUND;
    }

    public function getStatusMessage(): string
    {
        return __('errors.post_not_found');
    }
}
