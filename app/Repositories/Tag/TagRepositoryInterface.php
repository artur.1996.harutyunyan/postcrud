<?php

namespace App\Repositories\Tag;

interface TagRepositoryInterface
{
    public function getIdsByNames(array $names): array;
}
