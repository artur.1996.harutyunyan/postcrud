<?php

namespace App\Repositories\Tag;

use App\Models\Tag;

class EloquentTagRepository implements TagRepositoryInterface
{
    public function getIdsByNames(array $names): array
    {
        $query = Tag::query();

        $query->whereIn('name', $names);

        return $query->pluck('id')->toArray();
    }
}
