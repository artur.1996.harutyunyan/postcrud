<?php

namespace App\Repositories\Post;

use App\Exceptions\DeletingErrorException;
use App\Exceptions\PostNotFoundException;
use App\Exceptions\SavingErrorException;
use App\Models\Post;
use App\Dto\Post\ListPostsDto;
use App\Repositories\Traits\PaginationTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class EloquentPostRepository implements PostRepositoryInterface
{
    use PaginationTrait;

    public function save(Post $post): bool
    {
        if (!$post->save()) {
            throw new SavingErrorException();
        }
        return true;
    }

    public function delete(Post $post): bool
    {
        if (!$post->delete()) {
            throw new DeletingErrorException();
        }
        return true;
    }

    public function findOrFail(int $id): Post
    {
        $post = Post::find($id);
        if (is_null($post)) {
            throw new PostNotFoundException();
        }

        return $post;
    }

    public function list(ListPostsDto $dto): LengthAwarePaginator
    {
        $query = Post::query();

        if (!empty($tags = $dto->filters->tags)) {
            $query->whereHas('tags', function (Builder $q) use ($tags) {
                $q->whereIn('name', $tags);
            });
        }

        return $this->paginate($query, $dto->pagination->page, $dto->pagination->perPage);
    }
}
