<?php

namespace App\Repositories\Post;

use App\Models\Post;
use App\Dto\Post\ListPostsDto;
use Illuminate\Pagination\LengthAwarePaginator;

interface PostRepositoryInterface
{
    public function save(Post $post): bool;
    public function delete(Post $post): bool;
    public function findOrFail(int $id): Post;
    public function list(ListPostsDto $dto): LengthAwarePaginator;
}
