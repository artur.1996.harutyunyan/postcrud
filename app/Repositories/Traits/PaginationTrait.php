<?php

namespace App\Repositories\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

trait PaginationTrait
{
    public function paginate(Builder $query, int $page, int $perPage): LengthAwarePaginator
    {
        return $query->paginate(
            $perPage,
            ['*'],
            'page',
            $page
        );
    }
}
