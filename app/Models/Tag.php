<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @method static Builder|self query()
 */
class Tag extends Model
{
    use HasFactory;
    use SoftDeletes;

    public static function create(string $name): self
    {
        $tag = new self();

        $tag->setName($name);

        return $tag;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
