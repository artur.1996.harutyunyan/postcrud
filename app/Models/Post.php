<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $text
 * @property int $tag_id
 * @property-read  Collection|Tag[] $tags
 * @method static Builder|self find($value)
 * @method static Builder|self query()
 */
class Post extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }

    public static function create(string $name, string $text): self
    {
        $post = new self();

        $post->setName($name);
        $post->setText($text);

        return $post;
    }

    public function edit(string $name, string $text): self
    {
        $this->setName($name);
        $this->setText($text);

        return $this;
    }

    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(Tag::class, 'post_tags');
    }
}
