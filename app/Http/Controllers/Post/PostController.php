<?php

namespace App\Http\Controllers\Post;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\CreatePostRequest;
use App\Http\Requests\Post\ListPostsRequest;
use App\Http\Requests\Post\UpdatePostRequest;
use App\Dto\Post\CreatePostDto;
use App\Dto\Post\ListPostsDto;
use App\Dto\Post\UpdatePostDto;
use App\Services\Post\CreatePostService;
use App\Services\Post\DeletePostService;
use App\Services\Post\ListPostService;
use App\Services\Post\PostService;
use App\Services\Post\UpdatePostService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected CreatePostService $createPostService;
    protected UpdatePostService $updatePostService;
    protected ListPostService $listPostService;
    protected DeletePostService $deletePostService;

    public function __construct(
        CreatePostService $createPostService,
        UpdatePostService $updatePostService,
        ListPostService $listPostService,
        DeletePostService $deletePostService
    ) {
        $this->createPostService = $createPostService;
        $this->updatePostService = $updatePostService;
        $this->listPostService = $listPostService;
        $this->deletePostService = $deletePostService;
    }

    public function list(ListPostsRequest $request): JsonResponse
    {
        $listPostsDto = new ListPostsDto([
            'pagination' => [
                'page' => $request->getPage(),
                'perPage' => $request->getPerPage() ?? PostService::POSTS_IN_PER_PAGE,
            ],
            'filters' => [
                'tags' => $request->getTags(),
            ],
        ]);
        $result = $this->listPostService->list($listPostsDto);

        return $this->response($result->toArray($request));
    }

    public function create(CreatePostRequest $request): JsonResponse
    {
        $createPostDto = new CreatePostDto([
            'name' => $request->getName(),
            'text' => $request->getText(),
            'tags' => $request->getTags(),
        ]);

        $result = $this->createPostService->create($createPostDto);

        return $this->response($result->toArray($request));
    }

    public function update(UpdatePostRequest $request): JsonResponse
    {
        $updatePostDto = new UpdatePostDto([
            'id' => $request->getId(),
            'name' => $request->getName(),
            'text' => $request->getText(),
            'tags' => $request->getTags(),
        ]);

        $result = $this->updatePostService->update($updatePostDto);

        return $this->response($result->toArray($request));
    }

    public function delete(int $id, Request $request): JsonResponse
    {
        $result = $this->deletePostService->delete($id);

        return $this->response($result->toArray($request));
    }
}
