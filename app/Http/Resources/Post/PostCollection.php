<?php

namespace App\Http\Resources\Post;

use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * @property AnonymousResourceCollection $collection
 */
class PostCollection extends ResourceCollection
{
    public function toArray($request): array
    {
        return [
            'posts' => $this->collection,
        ];
    }
}
