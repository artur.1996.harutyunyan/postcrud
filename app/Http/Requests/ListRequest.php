<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListRequest extends FormRequest
{
    const PAGE = 'page';
    const PER_PAGE = 'perPage';

    public function rules(): array
    {
        return [
            self::PAGE => 'nullable|numeric',
            self::PER_PAGE => 'nullable|numeric',
        ];
    }

    public function getPage(): int
    {
        return $this->get(self::PAGE) ?? 1;
    }

    public function getPerPage(): ?int
    {
        return $this->get(self::PER_PAGE);
    }
}
