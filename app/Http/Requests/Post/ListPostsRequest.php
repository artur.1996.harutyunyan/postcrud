<?php

namespace App\Http\Requests\Post;

use App\Http\Requests\ListRequest;

class ListPostsRequest extends ListRequest
{
    const TAGS = 'tags';

    public function rules(): array
    {
        return [
            self::TAGS => 'nullable|array',
        ];
    }

    public function getTags(): array
    {
        return $this->get(self::TAGS) ?? [];
    }
}
