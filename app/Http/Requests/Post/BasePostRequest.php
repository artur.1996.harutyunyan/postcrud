<?php

namespace App\Http\Requests\Post;

use App\Models\Tag;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BasePostRequest extends FormRequest
{
    const NAME = 'name';
    const TEXT = 'text';
    const TAGS = 'tags';

    public function rules(): array
    {
        return [
            self::NAME => 'required|string',
            self::TEXT => 'required|string',
            self::TAGS => [
                'nullable',
                'array',
                Rule::exists((new Tag())->getTable(), 'name'),
            ],
        ];
    }

    public function getName(): string
    {
        return $this->get(self::NAME);
    }

    public function getText(): string
    {
        return $this->get(self::TEXT);
    }

    public function getTags(): array
    {
        return $this->get(self::TAGS) ?? [];
    }
}
