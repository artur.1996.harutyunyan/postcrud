<?php

namespace App\Http\Requests\Post;

class UpdatePostRequest extends BasePostRequest
{
    public function getId(): int
    {
        return $this->route('id');
    }
}
