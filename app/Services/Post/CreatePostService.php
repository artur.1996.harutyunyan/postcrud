<?php

namespace App\Services\Post;

use App\Dto\Post\CreatePostDto;
use App\Http\Resources\Post\PostResource;
use App\Models\Post;

class CreatePostService extends PostService
{
    public function create(CreatePostDto $dto): PostResource
    {
        $post = Post::create($dto->name, $dto->text);
        $this->postRepository->save($post);

        $tagIds = $this->tagRepository->getIdsByNames($dto->tags);
        $post->tags()->attach($tagIds);

        return new PostResource($post);
    }
}
