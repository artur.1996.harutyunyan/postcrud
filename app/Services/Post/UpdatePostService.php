<?php

namespace App\Services\Post;

use App\Dto\Post\UpdatePostDto;
use App\Http\Resources\Post\PostResource;

class UpdatePostService extends PostService
{
    public function update(UpdatePostDto $dto): PostResource
    {
        $post = $this->postRepository->findOrFail($dto->id);
        $tagIds = $this->tagRepository->getIdsByNames($dto->tags);

        $post->edit($dto->name, $dto->text);
        $this->postRepository->save($post);

        $post->tags()->sync($tagIds);

        return new PostResource($post);
    }
}
