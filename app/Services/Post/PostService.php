<?php

namespace App\Services\Post;

use App\Repositories\Post\PostRepositoryInterface;
use App\Repositories\Tag\TagRepositoryInterface;

class PostService
{
    const POSTS_IN_PER_PAGE = 100;

    protected PostRepositoryInterface $postRepository;
    protected TagRepositoryInterface $tagRepository;

    public function __construct(
        PostRepositoryInterface $postRepository,
        TagRepositoryInterface $tagRepository
    ) {
        $this->postRepository = $postRepository;
        $this->tagRepository = $tagRepository;
    }
}
