<?php

namespace App\Services\Post;

use App\Dto\Post\ListPostsDto;
use App\Http\Resources\Post\PostCollection;
use App\Http\Resources\Post\PostResource;

class ListPostService extends PostService
{
    public function list(ListPostsDto $dto): PostCollection
    {
        $listPosts = $this->postRepository->list($dto);

        return new PostCollection(PostResource::collection($listPosts));
    }
}
