<?php

namespace App\Services\Post;

use App\Http\Resources\Post\PostResource;

class DeletePostService extends PostService
{
    public function delete(int $id): PostResource
    {
        $post = $this->postRepository->findOrFail($id);

        $this->postRepository->delete($post);

        $post->tags()->detach();

        return new PostResource($post);
    }
}
