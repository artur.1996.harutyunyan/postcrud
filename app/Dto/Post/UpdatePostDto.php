<?php

namespace App\Dto\Post;

class UpdatePostDto extends BasePostDto
{
    public int $id;
}
