<?php

namespace App\Dto\Post;

use App\Dto\Pagination\Pagination;
use Spatie\DataTransferObject\DataTransferObject;

class ListPostsDto extends DataTransferObject
{
    public Pagination $pagination;
    public Filters $filters;
}
