<?php

namespace App\Dto\Post;

use Spatie\DataTransferObject\DataTransferObject;

class BasePostDto extends DataTransferObject
{
    public string $name;
    public string $text;
    public array $tags;
}
