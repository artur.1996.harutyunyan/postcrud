<?php

namespace App\Dto\Post;

use Spatie\DataTransferObject\DataTransferObject;

class Filters extends DataTransferObject
{
    public array $tags;
}
