<?php

namespace App\Dto\Pagination;

use Spatie\DataTransferObject\DataTransferObject;

class Pagination extends DataTransferObject
{
    public int $page;
    public int $perPage;
}
